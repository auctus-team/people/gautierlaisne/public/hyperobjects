from setuptools import setup, find_packages

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='hyperobjects',
   version='0.0.1',
   description='Computation on hyperobjects and continuous deformations.',
   license="MIT",
   long_description=long_description,
   author='Gautier LAISNE',
   author_email='gautier.laisne@inria.fr',
   url="https://gitlab.inria.fr/auctus-team/people/gautierlaisne/public/hyperobjects/",
   packages=find_packages(),
   install_requires=['numpy', 'matplotlib', "scipy"]
)