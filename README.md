# Primitives

What are primitives? Inspired by Hilbert's axioms of Euclidean geometry, primitives are the following objects:
- Point
- Vector
- AffineSpace:
    - Line
    - Plane
    - HyperPlane
    - AffineSpace
- Round: 
    - Ellipsoid
    - Sphere
    - Ball
- (Bounded) Intervals:
    - Segment
    - Cube
    - Orthotope
    - Octahedron (SuperQuadrics e=1)
- Rounded Intervals:
    - SuperEllipsoid
    - Ellipsoid (and hyperellipsoid)
    - SuperQuadrics
    - Lamé Curves
- From Minkowski Sum:
    - Zonotope
    - Polytope
    - Super

## Common properties:
Primitive relations:
- Containment
- Orthogonal projection
- Incidence (intersection)
- Minkowski Sum (when applicable)
- Dual (orthogonal complement, polar)

But also:
- affine transformation
- area and volume estimation
- approximation of high dimensional objects by rounded primitives (Grünbaum)


### Intersection of affine subspaces
This is a non trivial part.

Consider the affine subspaces $E_1=p_1+U_1$ and $E_2=p_2+U_2$.

1. Compute a basis for $U_1\cap U_2$
2. Find a point $y$ such that $y \in E_1$ and $y\in E_2$:

So we have $y-x_1 \in U_1$ and $y-x_2\in U_2$.

This tells us that:
$\pi_{U_1}y=y$ and $\pi_{U_2}y=y$.

Hence, since $\pi_{U_1}x = x_1 + P_{U_1}(x-x_1)$, we have

$(I-P_{U_1})y = x_1 - P_{U_1}x_1$ and $(I-P_{U_2})y = x_2 - P_{U_2}x_2$

Use the pseudo inverse to find a $y$, which is the closest point to origin.