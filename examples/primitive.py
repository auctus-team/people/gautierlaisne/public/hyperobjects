from abc import ABC
import matplotlib.pyplot as plt
import matplotlib
import mpl_toolkits
import matplotlib.lines
import numpy as np
import scipy
from functools import cached_property
import itertools
from dataclasses import dataclass
from scipy.spatial import ConvexHull, HalfspaceIntersection
import pycapacity.human
import pycapacity.algorithms
import pycapacity.visual
from pycapacity.objects import Polytope as SkuricPolytope
from enum import Enum
from functools import cache

class HyperObjectTypes(Enum):
    Null = 0
    Point = 1
    Line = 2
    Plane = 3
    HyperPlane = 4
    VectorSpace = 5
    AffineSpace = 6
    PairPoints = 10
    Sphere = 11
    Ellipsoid = 12
    Cube = 20
    Orthotope = 21
    CrossPolytope = 22
    Zonotope = 23
    Polytope = 24

# To not lose
def polytope_as_projection_intersection(A, N, x_min, x_max, b=None):
    """The polytope defined by P = im(A).inter(N @ [x_min, x_max] + b)
    can be rewritten as P = N @ K.inter(x_min, x_max) 
    with K = coker(N.T @ coker(A)) + np.linalg.pinv(N) @ bias"""
    K = coker(N.T @ coker(A))
    if b:
        K = K + b

    return K

# Utility function for matrices
def rank(matrix):
    matrix = np.array(matrix)
    if matrix.size == 0:
        return 0
    return np.linalg.matrix_rank(matrix)

def corank(matrix):
    """Corank is complementary to the concept of the rank
    of a mathematical object, and may refer to the 
    dimension of the left nullspace of a matrix or the dimension of 
    the cokernel of a linear transformation of a vector space."""
    m, _ = np.array(matrix).shape
    return m - rank(matrix) # dim(im(A)) - dim(dual(im(A)))

def nullity(matrix):
    """The dimension of its null space."""
    matrix = scipy.linalg.null_space(matrix)
    if matrix.size == 0:
        return 0
    return np.linalg.matrix_rank(matrix)

def is_injective(matrix):
    return np.isclose(nullity(matrix), 0)

def is_surjective(matrix):
    return np.isclose(corank(matrix), 0)

def is_bijective(matrix):
    return is_injective(matrix) and is_surjective(matrix)

# Four fundamental subspaces
def im(A: np.ndarray or "HyperObject") -> "AffineSpace":
    if issubclass(type(A), HyperObject):
        return AffineSpace(A.matrix, A.location)
    return AffineSpace(A)

def ker(A: np.ndarray or "HyperObject") -> "AffineSpace":
    if issubclass(type(A), HyperObject):
        return AffineSpace(scipy.linalg.null_space(A.matrix), np.linalg.pinv(A.matrix) @ A.location)
    return AffineSpace(scipy.linalg.null_space(A))

def coker(A: np.ndarray or "HyperObject") -> "AffineSpace":
    """Orthogonal complement to image, in codomain."""
    if issubclass(type(A), HyperObject):
        return AffineSpace(scipy.linalg.null_space(A.matrix.T), A.location)
    return AffineSpace(scipy.linalg.null_space(A.T))

def coim(A: np.ndarray or "HyperObject") -> "AffineSpace":
    """Orthogonal complement to kernel, in domain."""
    if issubclass(type(A), HyperObject):
        return AffineSpace(A.matrix.T, np.linalg.pinv(A.matrix) @ A.location)
    return AffineSpace(A.T)

def domain(A: np.ndarray or "HyperObject") -> "AffineSpace":
    if issubclass(type(A), HyperObject):
        return A.domain
    return AffineSpace(np.identity(A.shape[1]))

def codomain(A: np.ndarray or "HyperObject") -> "AffineSpace":
    if issubclass(type(A), HyperObject):
        return A.codomain
    return AffineSpace(np.identity(A.shape[0]))

def dim(A: "HyperObject") -> int:
    if isinstance(A, AffineSpace or Ellipsoid):
        return A.dim
    raise TypeError("Can only evaluate the dimension of spaces!")

def codim(A: np.ndarray or "HyperObject") -> int:
    """Dimension of the orthogonal subspace of ker, in domain. 
    I.E, dim(domain) - dim(ker)"""
    return dim(domain(A)) - dim(ker(A))

def basis(A: np.ndarray or "HyperObject"):
    if issubclass(type(A), HyperObject):
        return A.basis
    return scipy.linalg.orth(A)

def orthogonal_complement(A: np.ndarray or "HyperObject"):
    if isinstance(A, AffineSpace):
        return A.orthogonal_complement
    return coker(A)

def equation_system(A: "AffineSpace") -> np.ndarray:
    """Returns a linear system of the form Sx = b corresponding to
    the given affine space."""

    S = basis(coker(A)).squeeze()
    b = S.dot(A.location.ravel())
    return S, b

def projection_matrix(A: np.ndarray or "HyperObject"):
    """Returns the orthogonal projector P onto the current space.
    P is a matrix m x m with m = dim(codomain(A))."""
    U = basis(A)
    return U @ np.linalg.inv(U.T @ U) @ U.T

def embedded_sphere_transformation(A: np.ndarray or "HyperObject"):
    """It corresponds to the transformation matrix such that the mapping is from 
    the unit sphere located in the codomain (and not in a higher-dim domain!).
    Used for determining the quadratic form and for display and sampling."""

    if isinstance(A, Ellipsoid | AffineSpace):
        """Useful to know how the sphere in dim is deformed: self = self.rotation @ np.diag(self.radii) @ Sphere(self.dim)"""
        A = A.matrix
        
    u, s, _ = np.linalg.svd(A)
    d = rank(A)
    s_ = np.zeros_like(u)
    s_[:d, :d] = np.diag(s[:d])
    return u @ s_

    
def quadratic_form(A: np.ndarray or "HyperObject"):
    """Compute Q such that Q = (L@L.T)^-1 of a linear transformation L which is the embedded version of A."""
    
    if isinstance(A, Ellipsoid | AffineSpace):
        """Useful to know how the sphere in dim is deformed: self = self.rotation @ np.diag(self.radii) @ Sphere(self.dim)"""
        A = A.matrix
        
    L = embedded_sphere_transformation(A)
    
    if rank(L) == L.shape[0]:
        Q = np.linalg.inv(L@L.T)
    else:
        Q = np.linalg.pinv(L@L.T)
    return Q

def radii(A: np.ndarray or "Ellipsoid" or "AffineSpace"):
    """Compute the radii of an ellipsoid of transformation A (this is not the quadratic form!)."""
    if isinstance(A, AffineSpace | Ellipsoid):
        return radii(A.matrix)
    else:
        Q = quadratic_form(A)
        s = np.linalg.svd(Q, compute_uv=False)
        return 1/np.sqrt(s[:rank(Q)])
    
def radius(A: np.ndarray or "Ellipsoid"):
    r = radii(A)
    if np.all(np.isclose(r, r[0])):
        return r[0]
    raise ValueError("The object is not a sphere! It does not have have a radius (but may have radii).")

def volume(obj: "Ellipsoid"):
    """
    If n = dim(obj), this function returns the n-dimensional volume of the object.
    It corresponds to the n-Lebesgue measure.
    
    The n-dimensional volume of a Euclidean ball or int(ellipsoid) in n-dimensional Euclidean space."""
    if isinstance(obj, Ellipsoid):
        n = dim(obj)
        det_A = np.prod(radii(obj))
        vol_unit_sphere = np.pi**(n/2) / gamma(n/2 + 1)
        return np.abs(det_A) * vol_unit_sphere
    raise TypeError(f"Volume is not defined for object {type(obj)}!")

def area(obj: "Ellipsoid"):
    """Area is computable for n-sphere only.
    It is not an analytic function of the dimension unfortunately (but volume is!)"""
    if obj.type() == HyperObjectTypes.Sphere:
        r = radius(obj)
        n = dim(obj)
        return 2 * r**(n-1) * np.pi**(n/2) / gamma(n/2)
    raise TypeError(f"Area is not defined for object {type(obj)}!")
    
def sample_sphere(dim, nb_points, method="uniform", seed=0):
    np.random.seed(seed)
    if method == "uniform":
        vec = np.random.uniform(-1, 1, size=(dim, nb_points))
    elif method == "normal":
        vec = np.random.randn(dim, nb_points)
    else:
        raise ValueError("Method of sampling is not recognized.")
    vec /= np.linalg.norm(vec, axis=0)
    return vec

ZERO_TOLERANCE = 1e-12
def gamma(n):
    return scipy.special.gamma(n)

@dataclass(init=False, frozen=True)
class HyperObject:
    __array_priority__ = 10000 # To override numpy multiplication
    matrix: np.ndarray
    location: np.ndarray = None
    
    def __init__(self, matrix, location = None) -> None:
        """A HyperObject is defined as a continuous linear deformation. The caracterization
        of the underlying object results from what is deformed:
        - A Sphere deforms into an Ellipsoid or another Sphere
        - An AffineSpace deforms into another AffineSpace
        - A Cube deforms into an Orthotope"""
        matrix = np.array(matrix).squeeze()

        if matrix.ndim == 0:
            matrix = np.array([matrix])
        if matrix.ndim == 1:
            matrix = matrix.reshape((-1,1))
            
        if location is None:
            location = np.zeros(matrix.shape[0])
        location = np.array(location)

        if location.size != matrix.shape[0]:
            raise ValueError(f"The location should be of dimension {matrix.shape[0]}.")
        
        object.__setattr__(self, "matrix", matrix)
        object.__setattr__(self, "location", location.reshape((-1,1)))
    
    # AFFINE TRANSFORMATIONS
    def __add__(self, other):
        """Translate the hyperobject"""
        if not isinstance(other, list | np.ndarray | tuple):
            raise TypeError(f"Cannot translate hyperobject by object of type {type(other)}! Please use a list to translate.")
        
        other = np.array(other).reshape((-1,1))
        if other.size != self.location.size:
            raise ValueError(f"Can only add a vector of dimension {self.location.size} to this hyperobject!")
        
        if isinstance(self, Ellipsoid):
            return Ellipsoid(self.matrix, self.location + other)
        elif issubclass(type(self), AffineSpace):
            return AffineSpace(self.matrix, self.location + other)
        elif issubclass(type(self), Zonotope):
            return Zonotope(self.matrix, self.location + other, tol=self.tol).automatic_downcast()
        else:
            raise NotImplementedError("Impossible to add this object!")
        
    def __radd__(self, other: np.ndarray):
        return self.__add__(other)
    
    def __sub__(self, other):
        return self + (-np.array(other))
    
    def __rmul__(self, a):
        """Scaling only"""
        if not isinstance(a, float | int):
            raise TypeError(f"Scaling of hyperobject by object of type {type(a)} is not possible! Please use a float or int.")
        
        if isinstance(self, Ellipsoid):
            return Ellipsoid(a * self.matrix, self.location)
        elif issubclass(type(self), AffineSpace):
            return AffineSpace(a * self.matrix, self.location)
        elif issubclass(type(self), Zonotope):
            return Zonotope(a * self.matrix, self.location, tol=self.tol).automatic_downcast()
        else:
            raise NotImplementedError("Impossible to right multiply this object!")
        
    def __matmul__(self, _):
        raise NotImplementedError("Cannot transform an hyperobject on the right!")
    
    def __rmatmul__(self, other):
        if not isinstance(other, np.ndarray | list):
            raise TypeError(f"Cannot transform hyperobject by object of type {type(other)}! Please use a numpy array or a list.")
        other = np.array(other)
        
        if isinstance(self, Ellipsoid):
            return Ellipsoid(other @ self.matrix, other @ self.location)
        if issubclass(type(self), AffineSpace):
            return AffineSpace(other @ self.matrix, other @ self.location)
        elif issubclass(type(self), Zonotope):
            return Zonotope(other @ self.matrix, self.location, tol=self.tol).automatic_downcast()
        else:
            raise NotImplementedError("Impossible to matmul this object!")
    
    def project(self, other):
        pass

    def inter(self, other):
        pass

    def contains(self, other):
        pass
    
    def sum(self, other):
        pass

    def plot(self, *args, **kwargs):
        plot = None
        if len(args) == 0:
            pass
        elif len(args) == 1:
            plot = args[0]
        else:
            raise Exception("Args not valid!")
        
        if "plot" in kwargs:
            plot = kwargs["plot"]
            del kwargs["plot"]
        else:
            if plot is None:
                plot = plt
        
        # figure out what axes to plot on
        if plot == matplotlib.pyplot:
            # if its plt provided, get the current axes
            # see if the first one is already an axis, if not create one
            axes = plt.gcf().get_axes()
            if dim(codomain(self))  in [1,2]:
                if len(axes) != 0 and isinstance(axes[0], matplotlib.axes.Axes):
                    ax = axes[0]
                else:
                    ax = plt.axes()
            else:
                if len(axes) != 0 and isinstance(axes[0], mpl_toolkits.mplot3d.axes3d.Axes3D):
                    ax = axes[0]
                else:
                    ax = plt.axes(projection='3d')
        elif isinstance(plot, matplotlib.figure.Figure):
            # if its figure provided, get the current axes
            # see if the first one is already an axis, if not create one
            axes = plot.get_axes()
            if dim(codomain(self)) in [1,2]:
                if len(axes) != 0 and isinstance(axes[0], matplotlib.axes.Axes):
                    ax = axes[0]
                else:
                    ax = plot.add_subplot(111)
            else:
                if len(axes) != 0  and isinstance(axes[0], mpl_toolkits.mplot3d.axes3d.Axes3D):
                    ax = axes[0]
                else:
                    ax = plot.add_subplot(111, projection='3d')

        elif dim(codomain(self)) in [1,2] and isinstance(plot, matplotlib.axes.Axes):
            #if its axes provided, use it
            ax = plot
        elif dim(codomain(self)) == 3 and isinstance(plot, mpl_toolkits.mplot3d.axes3d.Axes3D):
            #if its axes provided, use it
            ax = plot
        else:
            print(f"no matplotlib {dim(codomain(self))}d axes provided")
            return plot
        return ax

    def sample(self, nb_points, method="uniform"):
        pass

    def copy(self):
        pass
    
@dataclass(frozen=True)
class Ellipsoid(HyperObject):
    def __init__(self, matrix: np.ndarray, location=None):
        super().__init__(matrix, location)
    
    def type(self):
        if dim(self) == 1:
            return HyperObjectTypes.PairPoints
        
        r = radii(self)
        if np.all(np.isclose(r, r[0])):
            return HyperObjectTypes.Sphere
        return HyperObjectTypes.Ellipsoid
        
    def is_translated(self) -> bool:
        """Returns True if this space has a locational part, False otherwise."""
        return not np.allclose(self.location, 0)
    
    def is_ellipsoid(self):
        return self.type() == HyperObjectTypes.Ellipsoid
    
    def is_sphere(self):
        return self.type() == HyperObjectTypes.Sphere
    
    def is_pair_point(self):
        """A specific type of ellipsoid which result from the uni-dimensional sphere 
        (the ball inscribed in this sphere is called segment)."""
        return dim(self) == 1
    
    def copy(self):
        return Ellipsoid(self.matrix.copy(), self.location.copy())
    
    def distance(self, other: "Ellipsoid", method=""):
        """Method can be: 'lower_bound', 'upper_bound'"""
        if not issubclass(type(other), Ellipsoid):
            raise TypeError(f"Cannot compute distance with object of type {type(other)}!")
        
        if id(self) == id(other):
            return 0 
        if method == "lower_bound":
            l1 = np.max(radii(self))
            l2 = np.max(radii(other))
            return 1/l1 + 1/l2
        elif method == "upper_bound":
            l1 = np.min(radii(self))
            l2 = np.min(radii(other))
            return 1/l1 + 1/l2
        else:
            raise ValueError("Method is not recognized!")

    def eval(self, X: np.ndarray):
        """Evaluate (X-c).T @ Q @ (X-c)"""
        X = np.array(X)

        if X.ndim == 1: X = X.reshape((-1,1))
        else:
            if X.shape[1] == dim(codomain(self)):
                X = X.T

        return self.eval_quadratic(X - self.location)
    
    def eval_quadratic(self, X, Y=None):
        X = np.array(X).squeeze()
        if X.ndim == 1: X = X.reshape((-1,1))
        else:
            if X.shape[1] == dim(codomain(self)):
                X = X.T

        if Y is None: Y = X
        else:
            Y = np.array(Y).squeeze()
            if Y.ndim == 1: Y = Y.reshape((-1,1))
            else:
                if Y.shape[1] == dim(codomain(self)):
                    Y = Y.T
        res = np.einsum('...i,...i->...', X.T.dot(quadratic_form(self)), Y.T)
        if len(res) == 1:
            return res[0]
        return res

    def inter_with_line(self, directions, locations=None):
        """Specific function for intersecting the ellipsoid with a line defined by a direction
        and a location. Multiple lines can be furnished for more optimized computations than self.inter(obj).
        
        The main difference with the function 'inter' is that the points returned are only points found. If
        a line has no intersection, is will return an empty list.
        This operation of intersecting an ellipsoid with lines is called Ellipsoid-Fan intersection."""
        directions = np.array(directions).squeeze()
        if directions.ndim == 1:
            directions = directions.reshape((-1,1))
        if locations is None:
            locations = np.zeros_like(directions)
        locations = np.array(locations).squeeze()
        if locations.ndim == 1:
            locations = locations.reshape((-1,1))

        a_ = self.eval(directions)
        b_ = 2*self.eval_quadratic(directions, locations - self.location)
        c_ = self.eval(locations - self.location) - 1
        delta = b_**2 - 4*a_*c_
        a2 = 2*a_ 
        
        delta_pos = delta[delta >= 0]
        dir_pos = directions[:, delta >= 0]
        loc_pos = locations[:,delta >= 0]
        b = b_[delta >= 0]
        sqrt_delta = np.sqrt(delta_pos)
        l1 = (-b - sqrt_delta)/a2
        l2 = (-b + sqrt_delta)/a2
        sols_pos = np.hstack([l1*dir_pos + loc_pos, l2*dir_pos + loc_pos])
        return sols_pos
        # return l1*dir_pos + loc_pos, l2*dir_pos + loc_pos
        
    def inter_with_affine_space(self, A: "AffineSpace"):
        if dim(codomain(A)) != dim(codomain(self)):
            raise ValueError("Objects do not live in the same ambiant space!")
        
        # check containment
        if dim(self) <= dim(A):
            k = A.contains(self.matrix)
            if k is True:
                return self
            
        # if A.is_line():
        #     return self.inter_with_line(A.matrix, A.location)
            
        C = embedded_sphere_transformation(self)
        # 1. Deforme the space to intersect it with the unit hypersphere 
        A_deformed = np.linalg.inv(C) @ (A + (-self.location))
        O = Sphere(dim(codomain(A))).location
        # The center of the new ellipsoid is simply the orthogonal projection of 0 to the deformed hyperplane
        center_inter = A_deformed.project(O)
        if np.linalg.norm(center_inter) >= 1:
            # The intersection is empty
            return None
        else:
            # Let's go back to the ellipsoid world.
            S_inter_A_deformed = basis(A_deformed) @ Sphere(dim(A), np.sqrt(1 - np.linalg.norm(center_inter)**2)) + center_inter
            E_inter_A = C @ S_inter_A_deformed + self.location
            return E_inter_A

    def inter(self, obj):
        """If you want to intersect with a specific object, please use the associated function
        for faster computation:
        - The intersection with line give a 'pair point'
        - Intersection with line: inter_with_line(dir, loc)"""
        if isinstance(obj, list):
            if all(isinstance(x, Line) for x in obj):
                # More efficient when using numpy concatenated operations
                directions = np.array([x.direction.reshape(-1) for x in obj]).T
                locations = np.array([x.location.reshape(-1) for x in obj]).T
                return self.inter_with_line(directions, locations)
            else:
                res = [self.inter(x) for x in obj]
                return res
        if isinstance(obj, AffineSpace):
            return self.inter_with_affine_space(obj)
        else:
            raise TypeError(f"Cannot intersect ellipsoid type {type(obj)}!")

    def plot(self, *args, **kwargs):
        ax = super().plot(*args, **kwargs)
            
        if dim(codomain(self)) == 2:
            if dim(self) == 1: # Pair-point
                L = embedded_sphere_transformation(self)[:,0].ravel()
                L = L / np.linalg.norm(L)
                
                r = radius(self)
                p1 = r * L + self.location.ravel()
                p2 = -r * L + self.location.ravel()
                ax.scatter(*p1, **kwargs)
                ax.scatter(*p2, **kwargs)
            elif dim(self) == 2: # Ellipse
                L = embedded_sphere_transformation(self)
                
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.cos(theta)
                y = np.sin(theta)
                xy = np.vstack([x,y])
                x,y = L @ xy + self.location
                
                ax.plot(x, y, **kwargs)

        elif dim(codomain(self)) == 3:
            if dim(self) == 1: # Pair-point
                L = embedded_sphere_transformation(self)[:,0].ravel()
                L = L / np.linalg.norm(L)
                
                r = radius(self)
                p1 = r * L + self.location.ravel()
                p2 = -r * L + self.location.ravel()
                ax.scatter(*p1, **kwargs)
                ax.scatter(*p2, **kwargs)
            elif dim(self) == 2: # Ellipse
                # Here we display a 2D-ellipsoid embedded in 3D
                L = embedded_sphere_transformation(self)
                
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.cos(theta)
                y = np.sin(theta)
                z = np.zeros_like(x)
                xy = np.vstack([x,y,z])
                x,y,z = L @ xy + self.location
                
                ax.plot(x, y, z, **kwargs)
            elif dim(self) == 3: # Ellipsoid
                L = embedded_sphere_transformation(self)
                
                # Set of all spherical angles:
                u = np.linspace(0, 2 * np.pi, 30)
                v = np.linspace(0, np.pi, 30)

                # Cartesian coordinates that correspond to the spherical angles:
                # (this is the equation of a 3-sphere):
                x = np.outer(np.cos(u), np.sin(v))
                y = np.outer(np.sin(u), np.sin(v))
                z = np.outer(np.ones_like(u), np.cos(v))

                for i in range(len(x)):
                    for j in range(len(x)):
                        [x[i,j],y[i,j],z[i,j]] = np.dot(L, [x[i,j],y[i,j],z[i,j]]) + self.location.flatten()

                ax.plot_surface(x, y, z, **kwargs)

        else:
            raise Exception(f"Cannot plot a {dim(self)}-ellipsoid in a {dim(codomain(self))}-dimensional space!")
        
class Sphere:
    def __new__(cls, dim, r=1, location=None):
        """A sphere is defined by its ambiant dimension, a radius and a location."""
        return Ellipsoid(r * np.identity(dim), location)
    
@dataclass(frozen=True)
class AffineSpace(HyperObject):
    def __init__(self, matrix: np.ndarray, location=None):
        super().__init__(matrix, location)
        
    def is_translated(self) -> bool:
        """Returns True if this space has a locational part, False otherwise."""
        return not np.allclose(self.location, 0)

    def is_trivial(self) -> bool:
        """Returns True if the vector space associated to this space
        is the 0 space, False otherwise."""
        return np.allclose(self.matrix, 0, atol=ZERO_TOLERANCE)
    
    def is_vector_space(self) -> bool:
        """Returns True if this affine space is a vector space (no locational part), False otherwise."""
        return not self.is_translated()
    
    def is_null(self) -> bool:
        """Returns True if this affine space is the null space, False otherwise."""
        return self.is_trivial() and not self.is_translated()
    
    def is_point(self) -> bool:
        """Returns True if this affine space is a point, False otherwise."""
        return self.is_trivial() and self.is_translated()
    
    def is_line(self) -> bool:
        """Returns True if this affine space is a line, False otherwise."""
        return dim(self) == 1
    
    def is_plane(self) -> bool:
        """Returns True if this affine space is a plane, False otherwise."""
        return dim(self) == 2
    
    def is_hyperplane(self) -> bool:
        """Returns True if this affine space is an hyperplane, False otherwise."""
        return dim(self) == (dim(codomain(self)) - 1)
    
    def type(self):
        if self.is_null():
            return HyperObjectTypes.Null
        elif self.is_point():
            return HyperObjectTypes.Point
        elif self.is_line():
            return HyperObjectTypes.Line
        elif self.is_plane():
            return HyperObjectTypes.Plane
        elif self.is_hyperplane():
            return HyperObjectTypes.HyperPlane
        elif self.is_vector_space():
            return HyperObjectTypes.VectorSpace
        else:
            return HyperObjectTypes.AffineSpace
    
    @cached_property
    def is_injective(self) -> bool:
        return is_injective(self.matrix)

    @cached_property
    def is_surjective(self) -> bool:
        return is_surjective(self.matrix)

    @cached_property
    def is_bijective(self) -> bool:
        return self.is_injective and self.is_surjective

    @cached_property
    def dim(self):
        return rank(self.matrix)
    
    @cached_property
    def codim(self):
        """Dimension of orthogonal complement of image"""
        return self.matrix.shape[0] - self.dim
    
    @cached_property
    def nullity(self):
        """Dimension of kernel"""
        return nullity(self.matrix)
    
    @cached_property
    def domain(self) -> "AffineSpace":
        return AffineSpace(self.domain_matrix)
    
    @cached_property
    def domain_matrix(self):
        return np.identity(self.shape[1])
    
    @cached_property
    def codomain(self) -> "AffineSpace":
        return AffineSpace(self.codomain_matrix)
    
    @cached_property
    def codomain_matrix(self):
        return np.identity(self.shape[0])
    
    @cached_property
    def basis(self):
        return scipy.linalg.orth(self.matrix)
    
    @cached_property
    def pinv_matrix(self):
        return np.linalg.pinv(self.matrix)
    
    @cached_property
    def ker_matrix(self):
        return scipy.linalg.null_space(self.matrix)
    
    @cached_property
    def ker(self) -> "AffineSpace":
        return AffineSpace(self.ker_matrix, self.pinv_matrix @ self.location)
    
    @cached_property
    def coker_matrix(self):
        return scipy.linalg.orth(self.matrix.T)
    
    @cached_property
    def orthogonal_complement(self):
        return AffineSpace(self.coker_matrix, self.location)


    # @cached_property
    # def equation_system(self):

    
    def copy(self):
        return AffineSpace(self.matrix.copy(), self.location.copy())
    
    def __and__(self, other):
        return self.inter(other)
    
    def inter(self, other: "AffineSpace" or "Ellipsoid"):
        if isinstance(other, Ellipsoid):
            return other.inter(self)
        
        if dim(codomain(self)) != dim(codomain(other)):
            raise ValueError("Both affine spaces do not live in the same ambiant space!")
        
        # First compute intersection of self and other as vector subspaces
        U = basis(self)
        W = basis(other)
        A = np.hstack([U, -W])
        NA = scipy.linalg.null_space(A)
        W = U @ NA[:dim(self), :] # W @ NA[self.dim:, :]
        if W.size != 0:
            W = W / np.linalg.norm(W, axis=0)     

        # now find the closest point in this intersection
        U1 = projection_matrix(self)
        U2 = projection_matrix(other)
        U = np.vstack([np.identity(U1.shape[0])-U1, np.identity(U2.shape[0])-U2])
        x1 = self.location
        x2 = other.location
        X = np.vstack([x1-U1@x1, x2-U2@x2])

        Y = np.linalg.pinv(U) @ X
        if W.size == 0:
            return AffineSpace(np.zeros(Y.size), Y)
        return AffineSpace(W, Y)
    
    def project(self, x: list or "AffineSpace"):
        """Orthogonal projection of a point, a set of points or an affine subspace."""
        if issubclass(type(x), AffineSpace):
            if x.is_null() or x.is_point():
                return Point(self.project(x.location))
            else:
                P = projection_matrix(self)
                return AffineSpace(P @ basis(x), P @ x.location + self.location)
        else:
            x = np.array(x).squeeze()
            if x.ndim == 1:
                x = np.array(x).reshape((-1,1))
            return projection_matrix(self) @ (x - self.location) + self.location
    
    def contains(self, x):
        """Check that a POINT (not vector!!) or a set of points are in the current space.
        For this, x is orthogonally projected onto the orthogonal subspace:
        - if the 0 vector is the result, then x is indeed in the current space. Returns True.
        - otherwise returns False.
        If x is a set of points, all points should be ordered in columns."""

        if is_surjective(self.matrix):
            # This means the orthogonal complement is empty
            return True
        if issubclass(type(x), HyperObject):
            # in this case, we will use vector comparison, and not point containment
            x = basis(x.matrix) + x.location
        else:
            x = np.array(x).squeeze()
            if x.ndim == 1:
                x = np.array(x).reshape((-1,1))
        if x.shape[0] != dim(codomain(self)):
            raise ValueError("The given point is not in the same codomain!")
        return np.allclose(projection_matrix(self) @ (x - self.location), x - self.location, atol=ZERO_TOLERANCE)
    
    def __contains__(self, x):
        return self.contains(x)
    
    def plot(self, *args, **kwargs):
        ax = super().plot(*args, **kwargs)

        scale = 1
        if "scale" in kwargs.keys():
            scale = kwargs["scale"]
            del kwargs["scale"]

        if dim(codomain(self)) == 1:
            # We place it on the x-axis
            if dim(self) == 0: # Point
                location = np.vstack([self.location, 0]).ravel()
                ax.scatter(*location, **kwargs)
            elif dim(self) == 1: # Line
                direction = np.vstack([basis(self).ravel(), 0]).ravel()
                location = np.vstack([self.location, 0]).ravel()
                ax.axline(location-direction, location+direction, **kwargs)
        
        elif dim(codomain(self)) == 2:
            if dim(self) == 0: # Point
                ax.scatter(*self.location, **kwargs)
            elif dim(self) == 1: # Line
                direction = basis(self).ravel()
                location = self.location.ravel()
                ax.axline(location-direction, location+direction, **kwargs)
            elif dim(self) == 2:
                print("The space is full-dimensional, so the space will not be displayed.")
        
        elif dim(codomain(self)) == 3:
            if dim(self) == 0: # Point
                ax.scatter(*self.location, **kwargs)
            if dim(self) == 1: # Line
                location = self.location.ravel()
                direction = basis(self).ravel()
                start, end = location - scale*direction, location + scale*direction
                data = np.vstack([start, end])
                ax.plot(*data.T, **kwargs)
            elif dim(self) == 2:
                # In this case, the affine space is a plane in 3D, so it
                # is an hyperplane
                x_lim = ax.get_xlim()
                y_lim = ax.get_ylim()

                x = 1 * np.linspace(*x_lim, 10)
                y = 1 * np.linspace(*y_lim, 10)

                xx, yy = np.meshgrid(x, y)
                XY = np.vstack([xx.ravel(), yy.ravel()])
                x, y, z = basis(self) @ XY + self.location
                
                x = x.reshape(xx.shape)
                y = y.reshape(xx.shape)
                z = z.reshape(xx.shape)
            
                ax.plot_surface(x, y, z, **kwargs)
            elif dim(self) == 3:
                print("The space is full-dimensional, so the space will not be displayed.")
        else:
            print(f"Cannot visualise data with dimension: {dim(self)}.")

class VectorSpace:
    def __new__(cls, matrix):
        return AffineSpace(matrix)

class Point:
    def __new__(cls, location):
        return AffineSpace(np.zeros_like(location), location)
    
class Line:
    def __new__(cls, matrix, location=None):
        A = AffineSpace(matrix, location)
        if dim(A) != 1:
            raise ValueError("The given matrix does not describe a line (1-dimensional)!")
        return A

class Plane:
    def __new__(cls, matrix, location=None):
        A = AffineSpace(matrix, location)
        if dim(A) != 2:
            raise ValueError("The given matrix does not describe a plane (2-dimensional)!")
        return A

class HyperPlane:
    def __new__(cls, coefs):
        """An hyperplane is the set of x such that:
        a1*x1 + a2*x2 + ... + an*xn + d = 0.
        The coefficients to give is an array [a1, a2, ..., an, d]"""
        coefs = np.array(coefs)
        if coefs.ndim != 1:
            raise ValueError("An hyperplane is define by a vector.")
        coefs = coefs.ravel()
        dir, d = coefs[:-1], coefs[-1]

        if np.isclose(d, 0):
            location = np.zeros_like(dir)
        else:
            # Get one non-zero coefficient in the direction
            i_coef_not_0 = np.transpose(dir.nonzero())
            if i_coef_not_0.size == 0:
                raise ValueError("At least one coefficient must be non-zero!")
            
            location = np.zeros_like(dir)
            location[i_coef_not_0[0]] = -d / dir[i_coef_not_0[0]]
        return coker(im(dir)) + location

@dataclass(frozen=True)
class Polytope:
    projection: AffineSpace = None
    intersection: AffineSpace = None

    def __init__(self, projection: AffineSpace=None, intersection: AffineSpace=None):
        """A polytope is a section of an hypercube: P = A.inter(C).
        A zonotope is a central section of an hypercube.
        A zonotope is also a projection of an hypercube.
        An orthotope is an affine transformation of an hypercube."""

        if projection is None and intersection is None:
            raise ValueError("Projection or intersection field should not be empty!")
        
        if projection is None:
            projection = AffineSpace(np.identity(intersection.dim_codomain))
        if intersection is None:
            intersection = AffineSpace(np.identity(projection.dim_codomain))
        object.__setattr__(self, "projection", projection)
        object.__setattr__(self, "intersection", intersection)

    @cached_property
    def dim(self):
        if self.intersection:
            return self.intersection.dim
        return self.projection.dim
    
    @property
    def dim_codomain(self):
        return self.intersection.dim_codomain
        
    @property
    def dim_domain(self):
        """Dimension of the cube."""
        return self.projection.dim_domain
    
    def cube(self):
        """Return the unit cube projected."""
        return Cube(self.projection.matrix[0])

    def copy(self):
        P = Polytope(self.A.copy())
        if self.projection is not None:
            P.projection = self.projection.copy()
        return P
            
    # P = A @ K.inter(Cube(50))
    def __matmul__(self, matrix: np.ndarray):
        matrix = np.array(matrix).squeeze()


@dataclass(frozen=True)
class Zonotope(HyperObject):
    """A zonotope is the affine transformation of an hypercube of R^d onto R^p, p < d."""

    tol: float | int = 1

    def __init__(self, matrix, location=None, tol=1):
        """Location here is an added translation to the projected center of the cube."""
        super().__init__(matrix, location)
        object.__setattr__(self, "tol", tol)

    def automatic_downcast(self):
        matrix = self.matrix

        b = np.zeros(matrix.shape)
        np.fill_diagonal(b, matrix.diagonal())
        is_diagonal = np.all(matrix == b)
        if self.dim == self.dim_codomain:
            if is_diagonal:
                if np.all(np.diag(matrix) == matrix[0,0]):
                    return Cube(self.dim, matrix[0,0], self.location, tol=self.tol)
            return Orthotope(self.matrix, self.location, tol=self.tol)
        return Zonotope(self.matrix, self.location, tol=self.tol)
    
    @cached_property
    def skuric_polytope(self) -> SkuricPolytope:
        A = np.identity(self.dim_codomain)
        C = Cube(self.dim_domain, tol=self.tol)
        b_min, b_max = C.bounds
        # bias_cube = self.matrix @ C.location
        vert, H, d, faces , F_vert, t_vert = pycapacity.algorithms.iterative_convex_hull_method(A, self.matrix, b_min, b_max, bias=-self.location, tol=self.tol)
        # return pycapacity.human.force_polytope(A.T, self.matrix, b_min, b_max, tol=self.tol)
        pol = SkuricPolytope(vertices=vert, H=H, d=d)
        return pol
    
    @cached_property
    def vertices(self):
        return self.skuric_polytope.vertices
    
    @cached_property
    def hyperplanes(self):
        return self.skuric_polytope.H, self.skuric_polytope.d
    
    def plot(self, *args, **kwargs):
        ax = super().plot(*args, **kwargs)

        if self.dim == 2 and self.dim_codomain == 2:
            pycapacity.visual.plot_polytope(polytope=self.skuric_polytope, plot=ax, **kwargs)
        elif self.dim == 2 and self.dim_codomain == 3:
            # Here we display a 2D-ellipsoid embedded in 3D
            pycapacity.visual.plot_polytope(self.skuric_polytope, plot=ax, **kwargs)
        elif self.dim == 2 and self.dim_codomain > 3:
            raise Exception(f"Cannot plot a {self.dim}-ellipsoid in a {self.dim_codomain}-dimensional space!")
        elif (self.dim == 3 and self.dim_codomain == 3):
            pycapacity.visual.plot_polytope(self.skuric_polytope, plot=ax, **kwargs)
        elif self.dim == 3 and self.dim_codomain > 3:
            raise Exception(f"Cannot plot a {self.dim}-ellipsoid in a {self.dim_codomain}-dimensional space!")
        else:
            print("cannot visualise data with dimension: "+str(self.dim))
    
    
@dataclass(frozen=True)
class Orthotope(Zonotope):
    """An orthotope is the affine transformation of an hypercube of R^d onto R^d."""
    def __init__(self, matrix, location=None, tol=1):
        super().__init__(matrix, location, tol)

        if dim(self) != dim(codomain(self)):
            raise ValueError("The given matrix is not full-rank! This should be a zonotope, not an orthotope.")
    
    @cached_property
    def side_lengths(self):
        return np.abs(np.diag(self.matrix))

    @cached_property
    def bounds(self) -> tuple:
        b_min = self.location.reshape(-1) - self.side_lengths / 2
        b_max = self.location.reshape(-1) + self.side_lengths / 2
        return b_min, b_max
    
    @staticmethod
    def from_bounds(bounds_min, bounds_max, tol=1):
        if len(bounds_min) != len(bounds_max):
            raise ValueError("Bounds are not of same length!")

        bounds_min = np.array(bounds_min)
        bounds_max = np.array(bounds_max)

        location = bounds_min + np.abs(bounds_max - bounds_min) / 2

        matrix = np.diag(np.abs(bounds_max - bounds_min))

        return Orthotope(matrix, location, tol=tol)    


# @dataclass(frozen=True)
# class Polytope:
#     A: AffineSpace
#     Z: Zonotope

#     def __init__(self, A: AffineSpace, Z: Zonotope):
#         """A polytope is constructed as the section of a zonotope Z with an affine subspace A.
#         If non-empty, it is a (dim(A))-polytope described in a (dim(Z))-affine space embedded in a (dim_domain(Z)) larger space."""
#         object.__setattr__(self, "A", A)
#         object.__setattr__(self, "Z", Z)

#     @cached_property
#     def dim(self):
#         return self.A.dim
    
#     @cached_property
#     def dim_domain(self):
#         return self.Z.dim_domain
        
#     @cached_property
#     def dim_codomain(self):
#         return self.Z.dim_codomain
    
#     @cached_property
#     def codim(self):
#         return self.dim_codomain - self.dim
    
#     @cached_property
#     def vertices(self):
#         """Co"""
#         pass



@dataclass(frozen=True)
class CrossPolytope(HyperObject):
    def __init__(self, dim: int, matrix=None, location=None):
        """A cross-polytope is defined as the set { x | sum_i( x_i b_i ) <= 1 }.
        The projection of a cross-polytope is a polytope.
        The intersection of a cross-polytope is a centrally symmetric polytope."""
        if matrix is None:
            matrix = np.identity(dim)
        super().__init__(matrix, location)
    
    @cached_property
    def vertices(self):
        """A n-dimensional cross-polytope has 2n vertices."""
        v = np.identity(self.dim)
        return np.hstack([self.matrix @ v + self.location, -self.matrix @ v + self.location])
    
@dataclass(frozen=True)
class Cube(Orthotope):
    side_length: float | int = 1

    def __init__(self, dim_codomain: int, side_length=1, location=None, tol=1):
        """A n-cube is the cartesian product [c - l/2, c + l/2]^n, 
        with c the center, l the side length."""
        matrix = side_length * np.identity(dim_codomain)
        super().__init__(matrix, location, tol)
        object.__setattr__(self, "side_length", side_length)


    @cached_property
    def volume(self):
        return self.side_length**dim(codomain(self))
    
    # @cached_property
    # def polytope(self):

    # @cached_property
    # def vertices(self):
    #     """Vertices are returnes as an iterator to avoid"""
    #     return np.array(list(itertools.product((-self.side_length/2, self.side_length/2), repeat=self.dim))).T + self.location
    
    # @cached_property
    # def hyperplanes(self):
    #     H = []
    #     for x in np.identity(self.dim):
    #         H.append(HyperPlane.from_normal(x, location=self.location.reshape(-1) + x))
    #         H.append(HyperPlane.from_normal(-x, location=self.location.reshape(-1) - x))
    #     return H
    
    def polar(self):
        """The polar is always convex.
        A** = closed(conv(A.union({O})))
        Using: https://www.cis.upenn.edu/~cis6100/convex2-09.pdf
        A** != A, except if A closed and convex, which is the case here."""
        
        # Step 1:
        H = self.hyperplanes
    # @property
    # def vertices(self):
    #     if self._vertices is not None:
    #         return self._vertices
    #     raise Exception("The function compute_vertices() should be called before calling vertices.")
    

    # def compute_vertices(self, method="iterative_convex_hull", **kwargs):
    #     """If method is 'iterative_convex_hull', you should set the argument tol."""
    #     if method == "iterative_convex_hull":
    #         bounds_min, bounds_max = self.bounds
    #         vertices, _, _, _, _, _ = pycapacity.algorithms.iterative_convex_hull_method(np.identity(self.dim), self.matrix, bounds_min, bounds_max, bias=self.location, **kwargs)
    #         hull = ConvexHull(vertices.T)
    #         vertices = hull.points[hull.vertices]
    #         self._vertices = vertices
    #     raise ValueError(f"Method {method} is not valid!")
    
    # def plot(self, *args, **kwargs):
    #     ax = super().plot(*args, **kwargs)

    #     vertices = self.vertices
    #     # P = pycapacity.objects.Polytope(vertices=vertices)
    #     # pycapacity.visual.plot_polytope()
    #     if self.dim == 2:            
    #         ax.scatter(vertices[0,:], vertices[1,:], **kwargs)  
    #     elif self.dim == 3:
    #         ax.scatter(vertices[0,:], vertices[1,:], vertices[2,:], **kwargs)
    #     else:
    #         print("cannot visualise data with dimension: "+str(self.dim))

# @dataclass(frozen=True)
# class Polytope:
#     """A polytope is an affine transformation of a polytope, or an intersection of a zonotope with an affine subspace."""
#     def __init__(self):
#         self.convex_hull = 
#         self.vertices = None
#         self.hyperplanes = None # list of [ [normal, d], [normal, d], ...]
#         # super().__init__(matrix, location)

#     @cached_property
#     def convex_hull(self) -> ConvexHull:
#         hull = ConvexHull(self.vertices.T, qhull_options='QJ')
#         return hull
    
#     @cached_property
#     def volume(self):
#         return self.convex_hull.volume

#     @staticmethod
#     def from_vertices(vertices):
#         P = Polytope()
#         P.vertices = vertices
#         return P
    
#     @staticmethod
#     def from_hyperplanes(H: list = [], d: list = None):
#         P = Polytope()

#         H = np.array(H)
#         if d is None:
#             d = np.zeros(H.shape[0])
#         P.hyperplanes = np.hstack([H, d.reshape(-1)])






    
