from abc import ABC
import matplotlib.pyplot as plt
import matplotlib
import mpl_toolkits
import matplotlib.lines
import numpy as np
import scipy
from functools import cached_property, cache
import pycapacity.human
import pycapacity.algorithms
import pycapacity.visual
from pycapacity.objects import Polytope as SkuricPolytope

ZERO_TOLERANCE = 1e-12
def gamma(n):
    return scipy.special.gamma(n)

def radius_grunbaum(n):
    """Radius of sphere An such that An = Proj(hypercube)"""
    if n > 300:
        ratio_gamma = (n/2)**(0-0.5)
    else:
        ratio_gamma = scipy.special.gamma(n/2)/scipy.special.gamma((n+1)/2)
    r_n = (n/np.sqrt(np.pi)) * ratio_gamma
    return r_n

class AffineMap:
    def __init__(self, matrix, translation=None) -> None:
        self.matrix = np.array(matrix)
        self.matrix[np.abs(self.matrix) < ZERO_TOLERANCE] = 0 # set to 0 small values
        if self.matrix.ndim == 1: # row
            self.matrix = self.matrix.reshape((-1,1))
        if translation is not None:
            translation = np.array(translation).ravel()
        else:
            translation = np.zeros(self.matrix.shape[0])
        if translation.size != self.matrix.shape[0]:
            raise ValueError("Translation is not in same ambiant space!")
        self.translation = translation

    def __matmul__(self, other: "AffineMap"):
        """Composition of fcts self @ other = self.matrix @ other.matrix"""
        return AffineMap(self.matrix @ other.matrix, self.matrix @ other.translation + self.translation)
    
    @cached_property
    def adjoint(self):
        return AffineMap(self.matrix.T, -np.linalg.pinv(self.matrix) @ self.translation)
    
    @cached_property
    def T(self):
        return self.adjoint
    
    @cached_property
    def rank(self):
        if self.matrix.size == 0:
            return 0
        return np.linalg.matrix_rank(self.matrix)
    
    @cached_property
    def corank(self):
        """Corank is complementary to the concept of the rank
        of a mathematical object, and may refer to the 
        dimension of the left nullspace of a matrix or the dimension of 
        the cokernel of a linear transformation of a vector space."""
        m, _ = np.array(self.matrix).shape
        return m - self.rank # dim(im(A)) - dim(dual(im(A)))
    
    @cached_property
    def nullity(self):
        """The dimension of its null space."""
        matrix = scipy.linalg.null_space(self.matrix)
        if matrix.size == 0:
            return 0
        return np.linalg.matrix_rank(matrix)
    
    def is_injective(self):
        return np.isclose(self.nullity, 0)

    def is_surjective(self):
        return np.isclose(self.corank, 0)

    def is_bijective(self):
        return self.is_injective() and self.is_surjective()
    
    @cached_property
    def pinv(self):
        return np.linalg.pinv(self.matrix)
    
    @cached_property
    def dim_domain(self):
        return self.matrix.shape[1]

    @cached_property
    def dim_codomain(self):
        return self.matrix.shape[0]
    
    # FOUR FUNDAMENTAL SUBSPACES
    def image(self):
        return AffineSpace(self.matrix, self.translation)
    
    def kernel(self):
        return AffineSpace(scipy.linalg.null_space(self.matrix), self.pinv @ self.translation)

    def coimage(self):
        return AffineSpace(self.matrix.T, self.pinv @ self.translation)

    def cokernel(self):
        return AffineSpace(scipy.linalg.null_space(self.matrix.T), self.translation)

    def __call__(self, X):
        """Evaluate"""
        if issubclass(type(X), list | np.ndarray | np.generic):
            # Corresponds to vector projection.
            X = np.array(X).squeeze()
            if X.ndim == 1:
                return self.matrix @ X + self.translation
            else:
                return self.matrix @ X + self.translation.reshape((-1,1))
        elif isinstance(X, AffineSpace):
            # Vector projection: X' = M@X + T
            # Point projection: X' = M@(X - T) + T
            if X.dim == 0: # Point, special case, no basis
                # return Point(self.matrix @ (X.location - self.translation) + self.translation)
                return Point(self.matrix @ (X.location - self.translation) + self.translation)
            
            # if self.rank <= X.dim:
            return AffineSpace(self.matrix @ X.basis, self.matrix @ X.location + self.translation)
        elif isinstance(X, Ellipsoid):
            return Ellipsoid(self.matrix @ X.generators, self.matrix @ X.location + self.translation)
        elif isinstance(X, Polytope):
            if X.is_zonotope():
                return Zonotope(self @ X.surjection)
            raise TypeError("Not possible to call an affine map onto a polytope! Only zonotopes are supported.")
        else:
            raise TypeError("Callable not implemented yet!")

def rank(M: AffineMap):
    return M.rank

def corank(M: AffineMap):
    return M.corank

def im(M: AffineMap):
    return M.image()

def ker(M: AffineMap):
    return M.kernel()
    
def coimage(M: AffineMap):
    return M.coimage()
    
def cokernel(M: AffineMap):
    return M.cokernel()

# FOR AFFINE SPACES, HELPERS
def dim(K):
    if isinstance(K, AffineSpace):
        return K.dim
    raise TypeError("Object type unrecognized!")

def codim(K):
    if isinstance(K, AffineSpace):
        return K.codim
    raise TypeError("Object type unrecognized!")

def basis(K):
    if isinstance(K, AffineSpace):
        return K.basis
    raise TypeError("Object type unrecognized!")

class HyperObject:
    def __init__(self) -> None:
        pass

    @property
    def dim_ambient(self):
        pass

    def plot(self, *args, **kwargs):
        plot = None
        if len(args) == 0:
            pass
        elif len(args) == 1:
            plot = args[0]
        else:
            raise Exception("Args not valid!")
        
        if "plot" in kwargs:
            plot = kwargs["plot"]
            del kwargs["plot"]
        else:
            if plot is None:
                plot = plt
        
        # figure out what axes to plot on
        if plot == matplotlib.pyplot:
            # if its plt provided, get the current axes
            # see if the first one is already an axis, if not create one
            axes = plt.gcf().get_axes()
            if self.dim_ambient  in [1,2]:
                if len(axes) != 0 and isinstance(axes[0], matplotlib.axes.Axes):
                    ax = axes[0]
                else:
                    ax = plt.axes()
            else:
                if len(axes) != 0 and isinstance(axes[0], mpl_toolkits.mplot3d.axes3d.Axes3D):
                    ax = axes[0]
                else:
                    ax = plt.axes(projection='3d')
        elif isinstance(plot, matplotlib.figure.Figure):
            # if its figure provided, get the current axes
            # see if the first one is already an axis, if not create one
            axes = plot.get_axes()
            if self.dim_ambient in [1,2]:
                if len(axes) != 0 and isinstance(axes[0], matplotlib.axes.Axes):
                    ax = axes[0]
                else:
                    ax = plot.add_subplot(111)
            else:
                if len(axes) != 0  and isinstance(axes[0], mpl_toolkits.mplot3d.axes3d.Axes3D):
                    ax = axes[0]
                else:
                    ax = plot.add_subplot(111, projection='3d')

        elif self.dim_ambient in [1,2] and isinstance(plot, matplotlib.axes.Axes):
            #if its axes provided, use it
            ax = plot
        elif self.dim_ambient == 3 and isinstance(plot, mpl_toolkits.mplot3d.axes3d.Axes3D):
            #if its axes provided, use it
            ax = plot
        else:
            print(f"no matplotlib {self.dim_ambient}d axes provided")
            return plot
        return ax

class AffineSpace(HyperObject):
    __array_priority__ = 10000 # To override numpy multiplication

    def __init__(self, generators, location=None):
        """You must be careful if you want to insert 
        - a row vector: 
        it should be of the form [[1,2,3,...]]
        - a column vector:
        it should be of the form [1,2,3,...] or [[1], [2], ..., []]"""
        super().__init__()

        generators = np.array(generators)
        generators[np.abs(generators) < ZERO_TOLERANCE] = 0 # set to 0 small values
        if generators.ndim == 1: # row
            generators = generators.reshape((-1,1))
        self.generators = generators
        self.basis = scipy.linalg.orth(generators)

        if location is not None:
            location = np.array(location).ravel()
        else:
            location = np.zeros(self.basis.shape[0])
        if location.size != self.basis.shape[0]:
            raise ValueError("Location is not in same ambiant space!")
        self.location = location
        
    def underlying_affine_map(self):
        return AffineMap(self.generators, self.location)

    @cached_property
    def dim(self):
        if self.basis.size == 0: # Point
            return 0
        return np.linalg.matrix_rank(self.basis)
    
    @cached_property
    def codim(self):
        if self.basis.size == 0: # Point
            return self.location.size
        return self.basis.shape[0] - self.dim
    
    @cached_property
    def dim_ambient(self):
        return self.basis.shape[0]
    
    @cached_property
    def orthogonal_complement(self):
        cokernel = scipy.linalg.null_space(self.generators.T)
        if cokernel.size == 0:
            # For example the orthogonal complement of R3 in R3 is nothing.
            return Point(self.location)
        else:
            return AffineSpace(cokernel, self.location)
    
    @cached_property
    def orthogonal_projection_matrix(self):
        U = self.basis
        return U @ np.linalg.inv(U.T @ U) @ U.T
    
    def project(self, other):
        """Project orthogonally a point or an affine space onto self"""
        U = self.orthogonal_projection_matrix
        
        if isinstance(other, AffineSpace):
            if other.is_point:
                return Point(self.location + U @ (np.asarray(other.location) - self.location))
            else:
                # we orthogonally project each generator onto the vector space, then we translate
                # A special case happens when the two spaces are orthogonal to each other
                if self.is_orthogonal(other):
                    return self & other
                else:
                    return AffineSpace(U @ other.generators, self.project(other.location))
        elif isinstance(type(other), Ellipsoid or Polytope):
            raise TypeError("Cannot project orthogonally an ellipsoid or a polytope onto this affine space!")
        else:
            # other is considered as a point if it's a list or else
            return self.location + U @ (np.asarray(other) - self.location)
        
    
    def __add__(self, other):
        """Translate the affine space"""
        other = np.array(other).ravel()
        if other.size != self.dim_ambient:
            raise ValueError(f"Can only add a vector of dimension {self.dim}!")
        
        if isinstance(self, AffineSpace):
            return AffineSpace(self.generators, self.location + other)
        raise NotImplementedError("Impossible to add this object!")
        
    def __radd__(self, other: np.ndarray):
        return self.__add__(other)
    
    def __sub__(self, other):
        if isinstance(other, AffineSpace):
            # Difference of 2 points = vector = line at 0
            if other.is_point and self.is_point:
                return Line(self.location - other.location)
            else:
                raise TypeError("Cannot substitute AffineSpace to self!")
        return self + (-np.array(other))
    
    def __rmul__(self, a):
        """Scaling only of the generators."""
        if not isinstance(a, float | int):
            raise TypeError(f"Scaling of hyperobject by object of type {type(a)} is not possible! Please use a float or int.")
        
        if isinstance(self, AffineSpace):
            return AffineSpace(a * self.generators, self.location)
        raise NotImplementedError("Impossible to right multiply this object!")
        
    def __rmatmul__(self, other):
        other = np.array(other)
        if isinstance(self, AffineSpace):
            return AffineSpace(other @ self.generators, other @ self.location)
        raise NotImplementedError("Impossible to matmul this object!")
    

    def __contains__(self, X):
        return self.contains(X)
    
    def is_parallel(self, other: "AffineSpace"):
        """Check if two subspaces are parallel."""
        if self.is_point:
            return True # everything is parallel to a point.
        elif isinstance(other, AffineSpace):
            if other.dim > self.dim:
                return other.is_parallel(self)
            V = AffineSpace(self.basis)
            for g in other.generators.T:
                if not np.allclose(V.project(g), g, atol=ZERO_TOLERANCE):
                    return False
            return True
        else:
            raise TypeError("Only affine spaces can be check for parallelness.")

    def is_orthogonal(self, other: "AffineSpace"):
        """Check if two subspaces are orthogonal."""
        if self.is_point:
            return True # everything is orthogonal to a point.
        elif isinstance(other, AffineSpace):
            # we check for parallelness over the orthogonal
            return (~self).is_parallel(other)
        else:
            raise TypeError("Only affine spaces can be check for parallelness.")

    def contains(self, X):
        """Check if a space or point is included inside self.
        If it's a space, then we simply need to check for parallelness and if location point lies in."""
        
        if isinstance(X, AffineSpace):
            if X.is_point:
                # Checking that a point X lies in a space means that the vector from a point
                # of self to X is included in space. i.e. is parallel.
                if np.allclose(X.location, self.location, atol=ZERO_TOLERANCE):
                    return True
                else:
                    L = Line(X.location - self.location)
                    return L.is_parallel(self)
            else:
                if np.allclose(X.location, self.location, atol=ZERO_TOLERANCE):
                    # If they have the same location, then we just need to check the parallelness
                    return X.is_parallel(self)
                else:
                    L = Line(X.location - self.location)
                    return X.is_parallel(self) and L.is_parallel(self)
        elif issubclass(type(X), list | np.ndarray | np.generic):
            X = np.array(X).squeeze()
            if X.shape[0] != self.dim_ambient:
                raise ValueError("The given point is not in the same ambiant space!")

            return self.contains(Point(X))
        else:
            raise TypeError(f"Cannot check containment with object of type {type(X)}.")
        
    def __invert__(self):
        return self.orthogonal_complement
    
    @cached_property
    def is_point(self):
        return self.generators.size == 0 or np.allclose(self.generators, 0, atol=ZERO_TOLERANCE)

    def __lshift__(self, other):
        """Project orthogonally other onto self."""
        return self.project(other)
    
    def __and__(self, other):
        return self.inter(other)
    
    def inter(self, other: "AffineSpace"):
        if isinstance(other, AffineSpace):
            if self.dim_ambient != other.dim_ambient:
                raise ValueError("Both affine spaces do not live in the same ambiant space!")
            
            # First compute intersection of self and other as vector subspaces
            U, W = self.basis, other.basis
            A = np.hstack([U, -W])
            NA = scipy.linalg.null_space(A)
            W = U @ NA[:self.dim, :] # W @ NA[self.dim:, :]
            if W.size != 0:
                W = W / np.linalg.norm(W, axis=0)     

            # now find the closest point in this intersection
            U1 = self.orthogonal_projection_matrix
            U2 = other.orthogonal_projection_matrix
            U = np.vstack([np.identity(U1.shape[0])-U1, np.identity(U2.shape[0])-U2])
            x1 = self.location.reshape((-1, 1))
            x2 = other.location.reshape((-1, 1))
            X = np.vstack([x1-U1@x1, x2-U2@x2])

            Y = np.linalg.pinv(U) @ X
            if W.size == 0: # Point
                return AffineSpace(np.zeros(Y.size), Y)
            return AffineSpace(W, Y)
        elif isinstance(other, Ellipsoid):
            return other.inter(self)
        elif isinstance(other, Polytope):
            return other.inter(self)
        else:
            raise TypeError(f"Cannot intersect AffineSpace and {type(other)}!")
    
    def copy(self):
        return AffineSpace(self.generators.copy(), self.location.copy())
    
    def plot(self, *args, **kwargs):
        ax = super().plot(*args, **kwargs)

        scale = 1
        if "scale" in kwargs.keys():
            scale = kwargs["scale"]
            del kwargs["scale"]

        dim = self.dim
        basis = self.basis
        location = self.location
        if self.dim_ambient == 1:
            # We place it on the x-axis
            if dim == 0: # Point
                location = np.hstack([location, 0])
                ax.scatter(*location, **kwargs)
            elif dim == 1: # Line
                direction = np.hstack([basis.ravel(), 0])
                location = np.hstack([location, 0])
                ax.axline(location-direction, location+direction, **kwargs)
        
        elif self.dim_ambient == 2:
            if dim == 0: # Point
                ax.scatter(*location, **kwargs)
            elif dim == 1: # Line
                direction = basis(self).ravel()
                ax.axline(location-direction, location+direction, **kwargs)
            elif dim == 2:
                print("The space is full-dimensional, so the space will not be displayed.")
        
        elif self.dim_ambient == 3:
            if dim == 0: # Point
                ax.scatter(*location, **kwargs)
            if dim == 1: # Line
                direction = basis.ravel()
                start, end = location - scale*direction, location + scale*direction
                data = np.vstack([start, end])
                ax.plot(*data.T, **kwargs)
            elif dim == 2:
                # In this case, the affine space is a plane in 3D, so it
                # is an hyperplane
                x_lim = ax.get_xlim()
                y_lim = ax.get_ylim()

                x = 1 * np.linspace(*x_lim, 10)
                y = 1 * np.linspace(*y_lim, 10)

                xx, yy = np.meshgrid(x, y)
                XY = np.vstack([xx.ravel(), yy.ravel()])
                x, y, z = basis @ XY + location.reshape((-1,1))
                
                x = x.reshape(xx.shape)
                y = y.reshape(xx.shape)
                z = z.reshape(xx.shape)
            
                ax.plot_surface(x, y, z, **kwargs)
            elif dim == 3:
                print("The space is full-dimensional, so the space will not be displayed.")
        else:
            print(f"Cannot visualise data with dimension: {dim(self)}.")


class VectorSpace:
    def __new__(cls, generators):
        return AffineSpace(generators)

class Point:
    def __new__(cls, location):
        return AffineSpace(np.zeros_like(location), location)
    
class Line:
    def __new__(cls, generators, location=None):
        A = AffineSpace(generators, location)
        if A.dim != 1:
            raise ValueError("The given generator does not describe a line (1-dimensional)!")
        return A

class Plane:
    def __new__(cls, generators, location=None):
        A = AffineSpace(generators, location)
        if A.dim != 2:
            raise ValueError("The given generators do not describe a plane (2-dimensional)!")
        return A

class HyperPlane:
    def __new__(cls, coefs):
        """An hyperplane is the set of x such that:
        a1*x1 + a2*x2 + ... + an*xn + d = 0.
        The coefficients to give is an array [a1, a2, ..., an, d]"""
        coefs = np.array(coefs)
        if coefs.ndim != 1:
            raise ValueError("An hyperplane should be define by a 1-dim list/array.")
        coefs = coefs.ravel()
        dir, d = coefs[:-1], coefs[-1]

        if np.isclose(d, 0):
            location = np.zeros_like(dir)
        else:
            # Get one non-zero coefficient in the direction
            i_coef_not_0 = np.transpose(dir.nonzero())
            if i_coef_not_0.size == 0:
                raise ValueError("At least one coefficient must be non-zero!")
            
            location = np.zeros_like(dir)
            location[i_coef_not_0[0]] = -d / dir[i_coef_not_0[0]]
        return AffineSpace(dir, location).orthogonal_complement


class Ellipsoid(HyperObject):
    __array_priority__ = 10000 # To override numpy multiplication

    def __init__(self, generators, location=None):
        """You must be careful if you want to insert 
        - a row vector: 
        it should be of the form [[1,2,3,...]]
        - a column vector:
        it should be of the form [1,2,3,...] or [[1], [2], ..., []]"""
        super().__init__()

        generators = np.array(generators)
        generators[np.abs(generators) < ZERO_TOLERANCE] = 0 # set to 0 small values
        if generators.ndim == 1: # row
            generators = generators.reshape((-1,1))
        self.generators = generators
        self.basis = scipy.linalg.orth(generators)

        if location is not None:
            location = np.array(location).ravel()
        else:
            location = np.zeros(self.basis.shape[0])
        if location.size != self.basis.shape[0]:
            raise ValueError("Location is not in same ambiant space!")
        self.location = location

    def underlying_affine_map(self):
        return AffineMap(self.generators, self.location)
    
    @staticmethod
    def lowner_john_outer_approximation(points, tol=0.0001):
        """
        Find the minimum volume ellipsoid enclosing (outside) a set of points.
        Return A, c where the equation for the ellipse given in "center form" is
        (x-c).T * A * (x-c) = 1.
        Points should be expressed in columns.
        """

        points = np.asmatrix(points).T
        N, d = points.shape
        Q = np.column_stack((points, np.ones(N))).T
        u = np.ones(N) / N
        err = 1 + tol
        while err > tol:
            X = Q * np.diag(u) * Q.T
            M = np.diag(Q.T * np.linalg.inv(X) * Q)
            jdx = np.argmax(M)
            step_size = (M[jdx] - d - 1.0) / ((d + 1) * (M[jdx] - 1.0))
            new_u = (1 - step_size) * u
            new_u[jdx] += step_size
            err = np.linalg.norm(new_u - u)
            u = new_u

        c = u * points  # center of ellipsoid
        Q = np.linalg.inv(points.T * np.diag(u) * points - c.T * c) / d

        return Ellipsoid.from_quadratic_form(Q, c)

    @staticmethod
    def from_quadratic_form(Q, c):
        c = np.asmatrix(c)
        Q = np.asmatrix(Q)

        LLT = np.linalg.inv(Q)
        u, s, _ = np.linalg.svd(LLT)
        A = u @ np.diag(np.sqrt(s))
        return Ellipsoid(A, location=c)

    
    @cached_property
    def dim(self):
        if self.basis.size == 0: # Point
            return 0
        return np.linalg.matrix_rank(self.basis)
    
    @cached_property
    def codim(self):
        if self.basis.size == 0: # Point
            return self.location.size
        return self.basis.shape[0] - self.dim
    
    @cached_property
    def dim_ambient(self):
        return self.basis.shape[0]
    
    @cached_property
    def embedded_sphere_transformation(self):
        """It corresponds to the transformation matrix such that the mapping is from 
        the unit sphere located in the codomain (and not in a higher-dim domain!).
        Used for determining the quadratic form and for display and sampling."""
        A = self.generators
        u, s, _ = np.linalg.svd(A)
        s_ = np.zeros_like(u)
        d = self.dim
        s_[:d, :d] = np.diag(s[:d])
        return u @ s_

    
    @cached_property
    def quadratic_form(self):
        """Compute Q such that Q = (L@L.T)^-1 of a linear transformation L 
        which is the embedded version of self."""
        L = self.embedded_sphere_transformation
        if self.dim == L.shape[0]:
            Q = np.linalg.inv(L@L.T)
        else:
            Q = np.linalg.pinv(L@L.T)
        return Q

    def is_pair_point(self):
        """A specific type of ellipsoid which result from the uni-dimensional sphere 
        (the ball inscribed in this sphere is called segment)."""
        return self.dim == 1
    
    def copy(self):
        return Ellipsoid(self.generators.copy(), self.location.copy())
    
    @cached_property
    def radii(self):
        """Compute the radii of an ellipsoid of transformation A (this is not the quadratic form!)."""
        Q = self.quadratic_form
        s = np.linalg.svd(Q, compute_uv=False)
        return 1/np.sqrt(s[:np.linalg.matrix_rank(Q)])
            
    @cached_property
    def radius(self):
        r = self.radii
        if np.all(np.isclose(r, r[0])):
            return r[0]
        raise ValueError("The object is not a sphere! It does not have have a radius (but may have radii).")

    @cached_property
    def volume(self):
        """
        If n = dim(obj), this function returns the n-dimensional volume of the object.
        It corresponds to the n-Lebesgue measure.
        
        The n-dimensional volume of a Euclidean ball or int(ellipsoid) in n-dimensional Euclidean space."""
        n = self.dim
        det_A = np.prod(self.radii)
        vol_unit_sphere = np.pi**(n/2) / gamma(n/2 + 1)
        return np.abs(det_A) * vol_unit_sphere
    
    def eval(self, X: np.ndarray):
        """Evaluate (X-c).T @ Q @ (X-c). So X is considered as a point here."""
        X = np.array(X)

        if X.ndim == 1: X = X.reshape((-1,1))
        else:
            if X.shape[1] == self.dim_ambient:
                X = X.T

        return self.eval_quadratic(X - self.location.reshape((-1,1)))
    
    def eval_quadratic(self, X, Y=None):
        X = np.array(X).squeeze()
        if X.ndim == 1: X = X.reshape((-1,1))
        else:
            if X.shape[1] == self.dim_ambient:
                X = X.T

        if Y is None: Y = X
        else:
            Y = np.array(Y).squeeze()
            if Y.ndim == 1: Y = Y.reshape((-1,1))
            else:
                if Y.shape[1] == self.dim_ambient:
                    Y = Y.T
        res = np.einsum('...i,...i->...', X.T.dot(self.quadratic_form), Y.T)
        if len(res) == 1:
            return res[0]
        return res
    
    def __add__(self, other):
        """Translate the ellipsoid/sphere"""
        other = np.array(other).ravel()
        if other.size != self.dim_ambient:
            raise ValueError(f"Can only add a vector of dimension {self.dim} to this ellipsoid!")
        
        if isinstance(self, Ellipsoid):
            return Ellipsoid(self.generators, self.location + other)
        else:
            raise NotImplementedError("Impossible to add this object!")
        
    def __radd__(self, other: np.ndarray):
        return self.__add__(other)
    
    def __sub__(self, other):
        return self + (-np.array(other))
    
    def __rmul__(self, a):
        """Scaling only"""
        if not isinstance(a, float | int):
            raise TypeError(f"Scaling of hyperobject by object of type {type(a)} is not possible! Please use a float or int.")
        
        if isinstance(self, Ellipsoid):
            return Ellipsoid(a * self.generators, self.location)
        raise NotImplementedError("Impossible to right multiply this object!")
        
    def __rmatmul__(self, other):
        other = np.array(other)
        if isinstance(self, Ellipsoid):
            return Ellipsoid(other @ self.generators, other @ self.location)
        raise NotImplementedError("Impossible to matmul this object!")
    

    def __and__(self, other):
        return self.inter(other)
    
    def inter_with_line(self, directions, locations=None):
        """Specific function for intersecting the ellipsoid with a line defined by a direction
        and a location. Multiple lines can be furnished for more optimized computations than self.inter(obj).
        
        The main difference with the function 'inter' is that the points returned are only points found. If
        a line has no intersection, is will return an empty list.
        This operation of intersecting an ellipsoid with lines is called Ellipsoid-Fan intersection."""
        directions = np.array(directions).squeeze()
        if directions.ndim == 1:
            directions = directions.reshape((-1,1))
        if locations is None:
            locations = np.zeros_like(directions)
        locations = np.array(locations).squeeze()
        if locations.ndim == 1:
            locations = locations.reshape((-1,1))

        a_ = self.eval(directions)
        b_ = 2*self.eval_quadratic(directions, locations - self.location.reshape((-1,1)))
        c_ = self.eval(locations - self.location.reshape((-1,1))) - 1
        delta = b_**2 - 4*a_*c_
        a2 = 2*a_ 
        
        delta_pos = delta[delta >= 0]
        dir_pos = directions[:, delta >= 0]
        loc_pos = locations[:,delta >= 0]
        b = b_[delta >= 0]
        sqrt_delta = np.sqrt(delta_pos)
        l1 = (-b - sqrt_delta)/a2
        l2 = (-b + sqrt_delta)/a2
        sols_pos = np.vstack([l1*dir_pos + loc_pos, l2*dir_pos + loc_pos])
        return sols_pos
        # return l1*dir_pos + loc_pos, l2*dir_pos + loc_pos
        

    def inter_with_affine_space(self, A: "AffineSpace"):
        if A.dim_ambient != self.dim_ambient:
            raise ValueError("Objects do not live in the same ambiant space!")
        
        # check containment
        if self.dim <= A.dim:
            if (self.basis in A) and (self.location in A):
                return self
                        
        C = self.embedded_sphere_transformation
        # 1. Deforme the space to intersect it with the unit hypersphere
        A_deformed = np.linalg.inv(C) @ (A + (-self.location))

        # A_deformed = np.linalg.inv(C) @ (A + (-self.location))
        O = Sphere(A.dim_ambient).location

        # The center of the new ellipsoid is simply the orthogonal projection of O to the deformed hyperplane
        center_inter = A_deformed << O # Orthogonal projection of point

        if np.linalg.norm(center_inter) >= 1:
            # The intersection is empty
            return None
        else:
            # Let's go back to the ellipsoid world.
            S = Sphere(A.dim, np.sqrt(1 - np.linalg.norm(center_inter)**2))
            S_inter_A_deformed = A_deformed.basis @ S + center_inter
            E_inter_A = C @ S_inter_A_deformed + self.location
            return E_inter_A
        
    def inter(self, other: "Ellipsoid" or AffineSpace):
        """If you want to intersect with a specific object, please use the associated function
        for faster computation:
        - The intersection with line give a 'pair point'
        - Intersection with line: inter_with_line(dir, loc)"""
        # if issubclass(other, list):
        #     if all(isinstance(x, Line) for x in other):
        #         # More efficient when using numpy concatenated operations
        #         directions = np.array([x.direction.reshape(-1) for x in other]).T
        #         locations = np.array([x.location.reshape(-1) for x in other]).T
        #         return self.inter_with_line(directions, locations)
        #     else:
        #         res = [self.inter(x) for x in other]
        #         return res
        if isinstance(other, AffineSpace):
            return self.inter_with_affine_space(other)
        else:
            raise TypeError(f"Cannot intersect ellipsoid type {type(other)}!")

    # def intersection_test(self, other: "Ellipsoid"):
    #     """Does a test to check if 2 ellipsoids intersect."""
    #     # https://math.stackexchange.com/questions/1114879/detect-if-two-ellipses-intersect
    #     # https://github.com/NickAlger/nalger_helper_functions/blob/master/tutorial_notebooks/ellipsoid_intersection_test_tutorial.ipynb
    #     from scipy.linalg import eigh
    #     from scipy.optimize import minimize_scalar

    #     def K_function(s, lambdas, v_squared, tau):
    #         return 1.-(1./tau**2)*np.sum(v_squared*((s*(1.-s))/(1.+s*(lambdas-1.))))
        
    #     Sigma_A = np.linalg.inv(self.A)
    #     lambdas, Phi = eigh(Sigma_A, b=Sigma_B)
    #     v_squared = np.dot(Phi.T, mu_A - mu_B) ** 2
    #     res = minimize_scalar(K_function,
    #                         bracket=[0.0, 0.5, 1.0],
    #                         args=(lambdas, v_squared, tau))
    #     return (res.fun[0] >= 0)



    def plot(self, *args, **kwargs):
        ax = super().plot(*args, **kwargs)

        if self.dim_ambient == 2:
            if self.dim == 1: # Pair-point
                L = self.embedded_sphere_transformation[:,0].ravel()
                L = L / np.linalg.norm(L)
                
                r = self.radius
                p1 = r * L + self.location
                p2 = -r * L + self.location
                ax.scatter(*p1, **kwargs)
                ax.scatter(*p2, **kwargs)
            elif self.dim == 2: # Ellipse
                L = self.embedded_sphere_transformation
                
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.cos(theta)
                y = np.sin(theta)
                xy = np.vstack([x,y])
                x,y = L @ xy + self.location.reshape((-1,1))
                
                ax.plot(x, y, **kwargs)

        elif self.dim_ambient == 3:
            if self.dim == 1: # Pair-point
                L = self.embedded_sphere_transformation[:,0].ravel()
                L = L / np.linalg.norm(L)
                
                r = self.radius
                p1 = r * L + self.location.ravel()
                p2 = -r * L + self.location.ravel()
                ax.scatter(*p1, **kwargs)
                ax.scatter(*p2, **kwargs)
            elif self.dim == 2: # Ellipse
                # Here we display a 2D-ellipsoid embedded in 3D
                L = self.embedded_sphere_transformation
                
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.cos(theta)
                y = np.sin(theta)
                z = np.zeros_like(x)
                xy = np.vstack([x,y,z])
                x,y,z = L @ xy + self.location.reshape((-1,1))
                
                ax.plot(x, y, z, **kwargs)
            elif self.dim == 3: # Ellipsoid
                L = self.embedded_sphere_transformation
                
                # Set of all spherical angles:
                u = np.linspace(0, 2 * np.pi, 30)
                v = np.linspace(0, np.pi, 30)

                # Cartesian coordinates that correspond to the spherical angles:
                # (this is the equation of a 3-sphere):
                x = np.outer(np.cos(u), np.sin(v))
                y = np.outer(np.sin(u), np.sin(v))
                z = np.outer(np.ones_like(u), np.cos(v))

                for i in range(len(x)):
                    for j in range(len(x)):
                        [x[i,j],y[i,j],z[i,j]] = np.dot(L, [x[i,j],y[i,j],z[i,j]]) + self.location.flatten()

                ax.plot_surface(x, y, z, **kwargs)

        else:
            raise Exception(f"Cannot plot a {self.dim}-ellipsoid in a {self.dim_ambient}-dimensional space!")
        
    
class Sphere:
    def __new__(cls, dim, r=1, location=None):
        """A sphere is defined by its ambiant dimension, a radius and a location."""
        return Ellipsoid(r * np.identity(dim), location)


def sample_sphere(dim, nb_points, method="uniform", seed=0):
    np.random.seed(seed)
    if method == "uniform":
        vec = np.random.uniform(-1, 1, size=(dim, nb_points))
    elif method == "normal":
        vec = np.random.randn(dim, nb_points)
    else:
        raise ValueError("Method of sampling is not recognized.")
    vec /= np.linalg.norm(vec, axis=0)
    return vec

class Polytope(HyperObject):
    __array_priority__ = 10000 # To override numpy multiplication

    def __init__(self, injection: AffineMap = None, surjection: AffineMap = None) -> None:
        """A polytope can be seen as a cube affine projection followed by an intersection with affine space."""
        # super().__init__()

        if injection is None and surjection is None:
            raise ValueError("The polytope is not well-defined!")
        elif injection is None and surjection is not None:
            injection = AffineMap(np.identity(surjection.dim_codomain))
        elif surjection is None and injection is not None:
            surjection = AffineMap(np.identity(injection.dim_codomain))
        
        if injection.dim_codomain != surjection.dim_codomain:
            raise ValueError("The polytope is not well-defined! The injection and surjection do not lead to the same ambient space.")
        if not injection.is_injective():
            raise ValueError("The injection map is not injective!")
        # if not surjection.is_surjective():
        #     raise ValueError("The surjection map is not surjective!")

        self.injection = injection
        self.surjection = surjection        

    @cached_property
    def dim_ambient(self):
        """Corresponds to the dimension where the polytope lives (intersection of im(injection) and surjection)"""
        if self.surjection is not None:
            return self.surjection.dim_codomain
        elif self.injection is not None:
            return self.injection.dim_codomain
    
    @cached_property
    def K_section(self) -> AffineSpace:
        """A polytope can also be described as a cube section followed by a projection,
        inste"""
        phi = self.injection
        psi = self.surjection

        K = ~psi.T(~im(phi))
        return K

    def is_zonotope(self):
        """A zonotope is the projection of a cube."""
        return self.injection.is_bijective()
        
    def is_orthotope(self):
        """An orthotope is an affine transformation of a hypercube."""
        return self.is_zonotope() and self.surjection.is_bijective()
    
    def is_cube(self):
        s = np.linalg.svd(self.surjection.matrix, compute_uv=False)
        return self.is_orthotope() and np.allclose(s, s[0], atol=ZERO_TOLERANCE)
    
    @cache
    def skuric_polytope(self, tol=0.1) -> SkuricPolytope:
        phi = self.injection
        psi = self.surjection
        
        bias = - (psi.translation - phi.translation)
        A = phi.matrix
        B = psi.matrix

        # C = Cube(self.dim_domain, tol=self.tol)
        b_min, b_max = np.array([[-0.5,0.5]] * psi.dim_domain).T
        vert, H, d, faces , F_vert, t_vert = pycapacity.algorithms.iterative_convex_hull_method(A, B, b_min, b_max, bias=bias, tol=tol)
        pol = SkuricPolytope(vertices=vert, H=H, d=d)
        pol.torque_vertices = t_vert
        pol.muscle_force_vertices = F_vert
        return pol
    
    def plot(self, *args, **kwargs):
        """ use in_intermediate_space=True to display the polytope in the injection/surjection codomain."""
        ax = super().plot(*args, **kwargs)

        tol = 0.1
        if "tol" in kwargs:
            tol = kwargs["tol"]
            del kwargs["tol"]

        pol = self.skuric_polytope(tol=tol)
        if "in_intermediate_space" in kwargs:
            if kwargs["in_intermediate_space"] is True:
                pol = SkuricPolytope(vertices=self.injection(pol.vertices))
            del kwargs["in_intermediate_space"]
        
        if "color" in kwargs:
            color = kwargs["color"]
            kwargs["face_color"] = color
            kwargs["edge_color"] = color

            del kwargs["color"]
            
        pycapacity.visual.plot_polytope(polytope=pol, plot=ax, **kwargs)


    def __add__(self, other: list):
        """Translation of a polytope according to the basis of im(self.injection).
        It is seen as the translation of the underlying zonotope by the translation self.injection(other)."""
        if isinstance(other, Polytope):
            """It will be expressed in self basis."""
            # new
            # new_injection = AffineMap(AffineSpace(np.hstack([im(self.injection).basis, im(other.injection).basis])).basis)
            new_surjection = AffineMap(np.hstack([self.surjection.matrix, other.surjection.matrix]), self.surjection.translation + other.surjection.translation)
            return Polytope(self.injection, new_surjection)
        else:
            other = np.array(other)
            new_surjection = AffineMap(self.surjection.matrix, self.surjection.translation + self.injection(other))
            return Polytope(self.injection, new_surjection)
        
    def __sub__(self, other: list):
        other = np.array(other)
        return self.__add__(-other)

    def __rmatmul__(self, other):
        if self.is_zonotope():
            # This step includes orthotopes and cubes as well.
            new_surj = AffineMap(other @ self.surjection.matrix, other @ self.surjection.translation)
            return Zonotope(new_surj)
        raise NotImplementedError("Not a zonotope, cube or orthotope.")
    
    def __rmul__(self, a):
        """Scaling of the polytope induces a scaling of surjection domain."""
        if not isinstance(a, float | int):
            raise TypeError(f"Scaling of hyperobject by object of type {type(a)} is not possible! Please use a float or int.")
        
        if self.is_zonotope():
            new_surj = AffineMap(a * self.surjection.matrix, self.surjection.translation)
            return Polytope(self.injection, new_surj)
        else:
            # new_surjection = AffineMap(a * self.surjection.matrix, a*self.surjection.translation)
            # return Polytope(self.injection, new_surjection) - (a-1)*self.injection.pinv @ self.surjection.translation
            raise NotImplementedError("Not a zonotope")
    
    def __and__(self, other):
        return self.inter(other)
    
    def inter(self, other):
        """Intersection occurs in the intermediate space!"""
        if isinstance(other, AffineSpace):
            if self.is_zonotope():
                return Polytope(AffineMap(other.generators, other.location), self.surjection)
            else:
                raise TypeError("Cannot intersect a non-zonotope with affine space!")
        else:
            raise TypeError("Cannot intersect these objects!")
            
    
class Cube:
    def __new__(cls, dim, side_length=1, location=None):
        surjection = AffineMap(side_length * np.identity(dim), location)
        return Polytope(surjection=surjection)

class Orthotope:
    def __new__(cls, bounds_min, bounds_max):
        bounds_min, bounds_max = np.array(bounds_min), np.array(bounds_max)
        surjection = AffineMap(np.diag(np.abs(bounds_max - bounds_min)), translation=bounds_min)
        return Polytope(surjection=surjection)
    
class Zonotope:
    def __new__(cls, surjection):
        return Polytope(surjection=surjection)
    